
# Cloud FirestoreのデータをPythonで取得・追加更新・削除する

## 準備

```sh
pip install firebase-admin #これ両方必要ない？ google-cloud-firestore だけでもいけそう
pip install google-cloud-firestore
```

### 認証ファイルをダウンロード
firebaseのコンソール画面を開きます。
https://console.firebase.google.com/

サービスアカウントを作成(Pythonにチェックをし、新しい秘密鍵の生成をクリック)。
秘密鍵（ファイル名：〜.json）をダウンロード。

## Load credentiial
```python
# 事前に export GOOGLE_APPLICATION_CREDENTIALS="[PATH]" でサービスアカウントを登録しておく必要があり
from google.cloud import firestore

db = firestore.Client()#.from_service_account_json('xxxx.json')
```
https://cloud.google.com/firestore/docs/quickstart-servers?hl=ja

もしくわ、こっちでもできる
```python
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("./〜.json") # ダウンロードした秘密鍵
firebase_admin.initialize_app(cred)
```

## ADD to Document
```python
# 自動生成された ID で追加する(add()) 基本こっちで問題ささそう
d = {
    'name': '太郎',
    'age': '134',
    'born': 1886
}
db.collection('users').add(d) #または doc_ref = db.collection('users').document(); doc_ref.set(d)

# domument id を指定して作る(set())
doc_ref = db.collection('users').document('Doc_id_of_taro')
doc_ref.set(d)

# set()は上書きにも使えるけど、全部上書いちゃうので(含まれてないフィールドは消えちゃう)基本的にはupdate使った方が良いかも
# ↓のようにやれば消えることなくマージされる
# city_ref.set({
#     u'capital': True
# }, merge=True)

# 因みにドキュメントを作成日で並べ替えるには、ドキュメントのフィールドとしてタイムスタンプを保存する必要があります。


# 様々なデータ型
import datetime
datatypes = {
    u'stringExample': u'Hello, World!',
    u'booleanExample': True,
    u'numberExample': 3.14159265,
    u'dateExample': datetime.datetime.now(),
    u'arrayExample': [5, True, u'hello'],
    u'nullExample': None,
    u'objectExample': {
        u'a': 5,
        u'b': True
    }
}

db.collection(u'data').document(u'one').set(datatypes)
```
https://cloud.google.com/firestore/docs/manage-data/add-data?hl=ja  
一括書き込みはこちら
https://cloud.google.com/firestore/docs/manage-data/transactions?hl=ja#batched-writes
## UPDATE to Document
```python
doc_ref = db.collection('users').document('Doc_id_of_taro')
#ageのみ更新される
doc_ref.update({'age': 15})
#新規でフィールド追加する場合
doc_ref.update({'Hight': 175})
# サーバーが更新を受信した時刻を追跡するサーバーのタイムスタンプを設定
doc_ref.update({'timestamp': firestore.SERVER_TIMESTAMP})
# ネストされたオブジェクトのフィールドを更新(favoritesは辞書(map))
user_ref.update({
    u'age': 13,
    u'favorites.color': u'Red'
})

#フィールド削除 存在しないとエラーになる
# doc_ref.delete('tall')
```
その他、配列の要素とか、、細かな更新方法はこちら
https://cloud.google.com/firestore/docs/manage-data/add-data?hl=ja#update-data
## READ by Reference of collection
```python
# Collection に対して reference 作って stream(), where()とかして読み取る
user_ref = db.collection('users')
docs = user_ref.stream()
for doc in docs:
    print('name:{} age:{}'.format(doc.get('name'), doc.get('age')))
    print(doc.to_dict())

#getは非推奨？
# docs = db.collection('users').get()
# for doc in docs:
#     print(doc.to_dict())

#条件指定  太郎が複数人いた場合、複数取れる。documentIDでは取れないの？
query = db.collection('users').where('name', '==', '太郎')
docs = query.get()
import pprint
for doc in docs:
    print(doc.to_dict())
#複数指定

#単一のフィールドに対しては比較演算子フィルタを複数回適用できる
.where('price', '>', 8000)
.where('price', '<=', 10000)
#詳細は要調査した方がよい
#下記のように等価演算子（==）と範囲比較（<、<=、>、>=）を組み合わせる場合はカスタムインデックスを作成する必要があります。(consoleとかから)
query = db.collection('users').where('active', '==', True).where('created_at', '>', datetime.datetime.now())
docs = query.get()
for doc in docs:
    print(doc.to_dict())
```

１回のクエリの中で、複数のフィルタを適用したものを複合クエリと呼びます。指定した検索条件全てを満たすドキュメントが取得できます。ただし、複合クエリにはフィルタの利用に関してさまざまな制約があります。 　等価演算子の利用には特に制限がありません。任意のフィールドに対して複数回フィルタを掛けることができます。

## DELETE to Document
```python
#存在してなくてもエラーにはならない
db.collection('users').document('Doc_id_of_taro').delete()
```

## Sort by document
orderBy()メソッドで先に指定したフィールドから順にソートの優先順位が高くなります。
.orderBy('date').get()
## limit()を使ってデータのページネーションを行う
