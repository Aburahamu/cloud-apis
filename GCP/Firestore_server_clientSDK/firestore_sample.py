# import firebase_admin
# from firebase_admin import credentials
# from firebase_admin import firestore

# cred = credentials.Certificate("/xxx.json") # ダウンロードした秘密鍵
# firebase_admin.initialize_app(cred)


# 事前に export GOOGLE_APPLICATION_CREDENTIALS="[PATH]" でサービスアカウントを登録しておく必要があり
from google.cloud import firestore
import pprint

db = firestore.Client()

# # Add or Update document
def _add_update():
    d = {
        'name': '太郎',
        'age': '134',
        'born': 1886
    }
    # Generate random Document ID
    db.collection('users').add(d)
    # # Or これでも良い
    # doc_ref = db.collection('users').document()
    # doc_ref.set(d)
    
    # Specific Document ID
    doc_ref = db.collection('users').document('Doc_id_of_taro')
    doc_ref.set(d)

    #n Updaten and Delete
    user_ref = db.collection('users').document('Doc_id_of_taro')
    user_ref.update({'age': 15})
    #新規でフィールド追加する場合
    user_ref.update({'Hight': 175})
    user_ref.update({'timestamp': firestore.SERVER_TIMESTAMP})
    #フィールド削除 存在しないとエラーになる
    # user_ref.delete('tall')

    import datetime
    datatypes = {
    u'stringExample': u'Hello, World!',
    u'booleanExample': True,
    u'numberExample': 3.14159265,
    u'dateExample': datetime.datetime.now(),
    u'arrayExample': [5, True, u'hello'],
    u'nullExample': None,
    u'objectExample': {
        u'a': 5,
        u'b': True
        }
    }
    db.collection(u'data').document(u'one').set(datatypes)

# read users
def _read():
    docs = db.collection('users').stream()
    print('[+] Display all list')
    for doc in docs:
        print('name:{} age:{}'.format(
            doc.get('name'), doc.get('age')))
        # print(doc.to_dict())
        print(f'{doc.id} => {doc.to_dict()}\n\n')


    docs = db.collection('users').where('name', '==', '太郎').get()
    print('[+] Search \'太郎\' by colum of name')
    for doc in docs:
        print(doc.to_dict())

# For delete()
def _delete():
    db.collection('users').document('Doc_id_of_taro').delete()
    print('[+] Delete Doc_id_of_taro... complete')

_add_update()
_read()
# _delete()