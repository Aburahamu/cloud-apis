実際に関数をデプロイする場合は、`main.py`にする必要がありそう
### for debug
linebotからのinputからの確認する場合はそれなりのデータ構造で送信する必要ありそうなのでその場合は要調査
```sh
# localにWebサーバ起動
functions-framework --target=fujiko3 --debug
# デフォルト main.py を見るぽいので ファイル指定する場合は --sourceを
functions-framework --target fujiko3 --source for_local_test.py --debug
# curlとかでテストしたり
curl localhost:8080 -X POST -d 'food 77'
```
### deploy
```sh
#deploy  .gcloudignore を書いとくと不必要なものをuploadしなくて良い。 
# --env-vars-file でファイルに書いてやる方がおすすめ。 
gcloud functions deploy fujiko3 --trigger-http --set-env-vars LINE_CHANNEL_SECRET=xxx,LINE_CHANNEL_ACCESS_TOKEN=pxxx --region us-central1
#boto3では以下の変数名で登録しておけば読み込んでくれる。
#AWS_SECRET_ACCESS_KEY
#AWS_ACCESS_KEY_ID
```
#### 詳細はこちら
https://scrapbox.io/techtechtech/Cloud_Function
