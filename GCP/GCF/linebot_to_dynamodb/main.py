import os
import base64, hashlib, hmac
import logging

from flask import abort, jsonify

from linebot import (
    LineBotApi, WebhookParser
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage
)

import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

import random, uuid
import datetime


dynamodb = boto3.resource('dynamodb', region_name='ap-northeast-1')
tbl = dynamodb.Table('Expenses')

def usage():
    return '''Usage:
food <cost> {<comment>}
cons <cost> {<comment>}
tran <cost> {<comment>}
dura <cost> {<comment>}
acti <cost> {<comment>}
heal <cost> {<comment>}
ls {<YY/MM>}
sum {<YY/MM>}
del <uniq id>'''

def insert(type, cost, comment):
    # boto3で勝手に読んでくれるのでこれは不要なはず
    # aws_key = os.environ.get('AWS_ACCESS_KEY_ID')
    # aws_secret = os.environ.get('AWS_SECRET_ACCESS_KEY')
    print('insert *****************')
    uniqid = str(uuid.uuid4())
    if type.lower() in 'food':
        type = 'food'
    elif type.lower() in 'cons':
        type = 'consumables'
    elif type.lower() in 'tran':
        type = 'transportation'
    elif type.lower() in 'acti':
        type =  'activity'
    elif type.lower() in 'dura':
        type =  'durables'
    elif type.lower() in 'heal':
        type = 'healthcare'
        
    item = {
        "uniqid": uniqid, #Partition key
        "cost": cost,
        "type": type,
        "date": str(datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')),
        "comment": comment
    }

    try:
        res = tbl.put_item(Item=item)
        # return res, item
        return uniqid
    except Exception as e:
        print(e)
    '''
    ちなみに書き込みには３パターンあるらしい
    DynamoDB.Client.put_item()
    DynamoDB.Table.put_item()
    DynamoDB.Table.batch_writer()
    https://michimani.net/post/aws-put-item-to-dynamodb-by-boto3/#dynamodbclientput_item
'''

def ls_by_month(yymm):
    print('ls*******************')
    items = []
    import re
    # yymm = re.sub('/', '-', yymm)
    key = {
     'FilterExpression': Key('date').begins_with(yymm)
    }
    while True:
        res = tbl.scan(**key)
        #res = tbl.scan(**kwargs)
        for item in res['Items']:
            items.append(item)
        if 'LastEvaluatedKey' not in res:
            break
        key.update(ExclusiveStartKey=res['LastEvaluatedKey'])
    return items

# //DynamoDBは項目のプライマリキー値を指定する必要があります。
def delete(uniqid=None):
    print('del*******************')
    try:
        res = tbl.delete_item(
            Key={
                'uniqid': uniqid
            }
            # ConditionExpression="info.rating <= :val",
            # ExpressionAttributeValues={
            #     ":val": Decimal(rating)
            # }
        )
    except ClientError as e:
        if e.response['Error']['Code'] == "ConditionalCheckFailedException":
            print(e.response['Error']['Message'])
        else:
            raise
    else:
        print(res)
        return res

def parse_data(text):
    import re
    from datetime import datetime

    reg_insert = re.compile('^(food|cons|tran|acti|heal|dura)\s(\d+)\s?([\d\w]+)?$', flags=re.IGNORECASE)
    reg_ls = re.compile('^ls\s?(\d{2}/\d{2})?$', flags=re.IGNORECASE)
    reg_sum = re.compile('^sum\s?(\d{2}/\d{2})?$', flags=re.IGNORECASE)
    reg_del = re.compile('^del\s([\w\d\-]+)$', flags=re.IGNORECASE)

    if reg_insert.match(text) is not None:
        parsed = reg_insert.match(text).groups()
        return parsed[0], parsed[1], parsed[2]
    elif reg_sum.match(text) is not None:
        parsed = reg_sum.match(text).groups()
        if parsed[0] is None:
            print('current month')
            return 'sum', datetime.strftime(datetime.today(), '%Y/%m'), 'dummy'
        else:
            print('specific month')
            return 'sum', "20{}".format(parsed[0]), 'dummy'
    elif reg_ls.match(text) is not None:
        parsed = reg_ls.match(text).groups()
        if parsed[0] is None:
            print('current month')
            return 'ls', datetime.strftime(datetime.today(), '%Y/%m'), 'dummy'
        else:
            print('specific month')
            return 'ls', "20{}".format(parsed[0]), 'dummy'
    elif reg_del.match(text) is not None:
        parsed = reg_del.match(text).groups()
        return 'del', parsed[0], 'dummy'
    else:
        return 'unknown' , 'dummy', 'dummy'


def fujiko3(request):
    channel_secret = os.environ.get('LINE_CHANNEL_SECRET')
    channel_access_token = os.environ.get('LINE_CHANNEL_ACCESS_TOKEN')

    line_bot_api = LineBotApi(channel_access_token)
    parser = WebhookParser(channel_secret)

    body = request.get_data(as_text=True)
    hash = hmac.new(channel_secret.encode('utf-8'),
        body.encode('utf-8'), hashlib.sha256).digest()
    signature = base64.b64encode(hash).decode()

    if signature != request.headers['X_LINE_SIGNATURE']:
        return abort(405)

    try:
        events = parser.parse(body, signature)
    except InvalidSignatureError:
        return abort(405)


    for event in events:
        if not isinstance(event, MessageEvent):
            continue
        if not isinstance(event.message, TextMessage):
            continue
        
        text = event.message.text
        type, value, comment = parse_data(text);

        if type == 'ls':
            records = ls_by_month(value)
            reply = '{}\n\n'.format(value)   
            temp = []
            for r in records:
                if 'comment' not in r:
                    temp.append("{},{}:{}\n".format(str(r['date'])[5:-3], str(r['type'])[0:4], str(r['cost'])))
                else:
                    temp.append("{},{}:{} {}\n".format(str(r['date'])[5:-3], str(r['type']), str(r['cost']), str(r['comment'])))
                    # reply += "{}, {}:{}\n".format(str(r['date'])[5:], str(r['type']), str(r['cost']))
            temp = sorted(temp, reverse=True)
            for r in temp:
                reply += r
        elif type == 'sum':
            from collections import Counter, defaultdict
            d = defaultdict(int)
            d_cnt = defaultdict(int)
            total = 0

            records = ls_by_month(value)
            reply = '{}\n\n'.format(value)

            for r in records:
                d[r['type']] += int(r['cost'])
                d_cnt[r['type']] += 1
                total += int(r['cost'])
            tpl = sorted(d.items(), key=lambda x:x[1], reverse=True)
            for k,v in tpl:
                reply += '{}({}):{}\n'.format(k, d_cnt[k], v)
            reply += '====================\nTotal:{}'.format(total)

        elif type == 'del':
            reply = 'まだ実装してないよ \nlinebot - GCF - DynamoDB'
        elif type == 'unknown':
            reply = usage()
        else:
            reply = type
            reply = insert(type, value, comment)

        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=reply.rstrip())
            # TextSendMessage(text=event.message.text)
        )
    return jsonify({ 'message': 'ok'})
    