
#署名とかあるのででバッグには使えないかも
#https://developers.line.biz/en/reference/messaging-api/#common-specifications

#!/bin/bash

json=$(cat << EOS
{
  "destination": "xxxxxxxxxx",
  "events": [
    {
      "replyToken": "0f3779fba3b349968c5d07db31eab56f",
      "type": "message",
      "mode": "active",
      "timestamp": 1462629479859,
      "source": {
        "type": "user",
        "userId": "U4af4980629..."
      },
      "message": {
        "id": "325708",
        "type": "text",
        "text": "Hello, world"
      }
    },
    {
      "replyToken": "8cf9239d56244f4197887e939187e19e",
      "type": "follow",
      "mode": "active",
      "timestamp": 1462629479859,
      "source": {
        "type": "user",
        "userId": "U4af4980629..."
      }
    }
  ]
}
EOS
)


curlcmd="curl -s \
    -X POST \
    -H \"X-Line-Signature: tokendayo\" \
    --data '$json' \
    localhost:8080"
response=`eval $curlcmd`
echo $response
