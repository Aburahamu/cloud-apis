import os
import base64, hashlib, hmac
import logging

from flask import abort, jsonify

from linebot import (
    LineBotApi, WebhookParser
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage
)

import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

import random, uuid
import datetime
# import faker


dynamodb = boto3.resource('dynamodb')
tbl = dynamodb.Table('Expenses')

def usage():
    return '''
Usage:
food <cost>
con <cost>
trans <cost>
activ <cost>
med <cost>
ls {<YY/MM>}
sum {<YY/MM>}
del <uniq id>
'''

def insert(type, cost):
    # boto3で勝手に読んでくれるのでこれは不要なはず
    # aws_key = os.environ.get('AWS_ACCESS_KEY_ID')
    # aws_secret = os.environ.get('AWS_SECRET_ACCESS_KEY')
    print('insert *****************')
    uniqid = str(uuid.uuid4())
    
    item = {
        "uniqid": uniqid, #Partition key
        "cost": cost,
        "type": type,
        "date": str(datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S')),
    }

    try:
        res = tbl.put_item(Item=item)
        # return res, item
        return uniqid
    except Exception as e:
        print(e)
    '''
    ちなみに書き込みには３パターンあるらしい
    DynamoDB.Client.put_item()
    DynamoDB.Table.put_item()
    DynamoDB.Table.batch_writer()
    https://michimani.net/post/aws-put-item-to-dynamodb-by-boto3/#dynamodbclientput_item
'''


def ls_by_month(yymm):
    print('ls*******************')
    items = []
    import re
    # yymm = re.sub('/', '-', yymm)
    key = {
     'FilterExpression': Key('date').begins_with(yymm)
    }
    while True:
        res = tbl.scan(**key)
        #res = tbl.scan(**kwargs)
        for item in res['Items']:
            items.append(item)
        if 'LastEvaluatedKey' not in res:
            break
        key.update(ExclusiveStartKey=res['LastEvaluatedKey'])
    return items

# //DynamoDBは項目のプライマリキー値を指定する必要があります。
def delete(uniqid=None):
    try:
        res = tbl.delete_item(
            Key={
                'uniqid': uniqid,
            },
            # ConditionExpression="info.rating <= :val",
            # ExpressionAttributeValues={
            #     ":val": Decimal(rating)
            # }
        )
    except ClientError as e:
        if e.response['Error']['Code'] == "ConditionalCheckFailedException":
            print(e.response['Error']['Message'])
        else:
            raise
    else:
        return res
    pass

def _parse_data(text):
    import re
    from datetime import datetime

    # msg = re.split('\s', text)
    reg_insert = re.compile('^(food|con|trans|active|med)\s(\d+)$', flags=re.IGNORECASE)
    reg_sum = re.compile('^sum\s?(\d{2}/\d{2})?$', flags=re.IGNORECASE)
    reg_ls = re.compile('^ls\s?(\d{2}/\d{2})?$', flags=re.IGNORECASE)
    reg_del = re.compile('^del\s([\w\d\-]+)$', flags=re.IGNORECASE)

    if reg_insert.match(text) is not None:
        parsed = reg_insert.match(text).groups()
        return parsed[0], parsed[1]
    elif reg_sum.match(text) is not None:
        parsed = reg_sum.match(text).groups()
        if parsed[0] is None:
            print('current month')
            return 'sum', datetime.strftime(datetime.today(), '%Y/%m')
        else:
            print('specific month')
            return 'sum', "20{}".format(parsed[0])
    elif reg_ls.match(text) is not None:
        parsed = reg_ls.match(text).groups()
        if parsed[0] is None:
            print('current month')
            return 'ls', datetime.strftime(datetime.today(), '%Y/%m')
        else:
            print('specific month')
            return 'ls', "20{}".format(parsed[0])
    elif reg_del.match(text) is not None:
        parsed = reg_del.match(text).groups()
        return 'del', parsed[0]
    else:
        return 'unknown' , 'dummy'


#curl localhost:8080 -X POST -d 'food 77' でテストできる
def fujiko3(req):
    print('start*********************')

    channel_secret = "wwwww"
    channel_access_token = "vvv"
    # channel_secret = os.environ.get('LINE_CHANNEL_SECRET')
    # channel_access_token = os.environ.get('LINE_CHANNEL_ACCESS_TOKEN')

    # line_bot_api = LineBotApi(channel_access_token)
    # parser = WebhookParser(channel_secret)

    text = req.get_data(as_text=True)
    print(text,'\n\n')
    
    # hash = hmac.new(channel_secret.encode('utf-8'), body.encode('utf-8'), hashlib.sha256).digest()
    # signature = base64.b64encode(hash).decode()

    # if signature != req.headers['X_LINE_SIGNATURE']:
    #     return abort(405)

    # try:
    #     events = parser.parse(body, signature)
    # except InvalidSignatureError:
    #     return abort(405)


    type, value = _parse_data(text);

    if type == 'ls':
        records = ls_by_month(value)
        reply = '{}\n'.format(value)   
        temp = []
        # print(records)
        for r in records:
            if 'comment' not in r:
                temp.append("{},{}:{}\n".format(str(r['date'])[5:-3], str(r['type']), str(r['cost'])))
            else:
                temp.append("{},{}:{} {}\n".format(str(r['date'])[5:-3], str(r['type']), str(r['cost']), str(r['comment'])))
                # reply += "{}, {}:{}\n".format(str(r['date'])[5:], str(r['type']), str(r['cost']))
        temp = sorted(temp, reverse=True)
        for r in temp:
            reply += r
        print(reply)
    elif type == 'sum':
        from collections import Counter, defaultdict
        d = defaultdict(int)
        d_cnt = defaultdict(int)
        total = 0

        records = ls_by_month(value)
        reply = '{}\n'.format(value)

        for r in records:
            d[r['type']] += int(r['cost'])
            d_cnt[r['type']] += 1
            total += int(r['cost'])
        print(d)
        tpl = sorted(d.items(), key=lambda x:x[1], reverse=True)
        for k,v in tpl:
            reply += '{}({}):{}\n'.format(k, d_cnt[k], v)

        reply += '====================\nTotal:{}'.format(total)

    elif type == 'del':
        reply = 'まだ実装してないよ \nlinebot - GCF - DynamoDB'
    elif type == 'unknown':
        reply = usage()
    else:
        reply = type
        reply = insert(type, value)

    print(reply)

    # for event in events:
    #     if not isinstance(event, MessageEvent):
    #         continue
    #     if not isinstance(event.message, TextMessage):
    #         continue
        
    #     # ここで受信したmsgをparseして処理を分けてやる。
    #     # text=event.message.tex
    #     # type, value = _parse_data(text);


    #     line_bot_api.reply_message(
    #         event.reply_token,
    #         TextSendMessage(text=event.message.text)
    #     )

    print('end***********************')

    # return jsonify({ 'message': 'ok'})
    return 'testtest'
